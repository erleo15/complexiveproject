package ec.edu.ups.logic;

import ec.edu.ups.model.NivelEducacion;
import ec.edu.ups.model.ProjectPlotIn;
import ec.edu.ups.model.ProjectPlotOut;
import ec.edu.ups.util.AccessControlFilter;
import ec.edu.ups.util.ResponseCode;
import ec.edu.ups.util.Tools;

import javax.enterprise.context.ApplicationScoped;
import java.util.*;

@ApplicationScoped
public class EducationLevelProccess {

    public ProjectPlotOut getEducationLevel(AccessControlFilter context, ProjectPlotIn plot) {
        List<NivelEducacion> listaNivelEducacion = new ArrayList<>();
        ProjectPlotOut result = new ProjectPlotOut(ResponseCode.ERROR_NOT_FOUND.getResponseCode(), plot.getUserApp(), context.getLogGeneral().getDateTrn(), UUID.randomUUID().toString(), context.getHttpServerRequest().absoluteURI(), ResponseCode.ERROR_NOT_FOUND.getMessage(), ResponseCode.ERROR_NOT_FOUND.getMessage(), null);
        if (Tools.verify(plot, "cNivelEducacion")) {
            try {
                listaNivelEducacion.add(NivelEducacion.findById(Integer.parseInt(plot.getCtl().get("cNivelEducacion").toString())));
            } catch (NumberFormatException e) {
                result = new ProjectPlotOut(ResponseCode.ERROR_NUMBERFORMAT_EX.getResponseCode(), plot.getUserApp(), context.getLogGeneral().getDateTrn(), UUID.randomUUID().toString(), context.getHttpServerRequest().absoluteURI(), ResponseCode.ERROR_NUMBERFORMAT_EX.getMessage(), ResponseCode.ERROR_NUMBERFORMAT_EX.getMessage(), null);
            }
        } else {
            listaNivelEducacion = NivelEducacion.findAll().list();
        }
        Map<String, Object> ctlObject = new LinkedHashMap<>();
        ctlObject.put("nivelEducacion", listaNivelEducacion);
        if (listaNivelEducacion.get(0) == null || listaNivelEducacion.isEmpty()) {
            result = new ProjectPlotOut(ResponseCode.ERROR_NOT_FOUND.getResponseCode(), plot.getUserApp(), context.getLogGeneral().getDateTrn(), UUID.randomUUID().toString(), context.getHttpServerRequest().absoluteURI(), ResponseCode.ERROR_NOT_FOUND.getMessage(), ResponseCode.ERROR_NOT_FOUND.getMessage(), null);
        } else {
            result = new ProjectPlotOut(ResponseCode.OK_RESPONSE.getResponseCode(), plot.getUserApp(), context.getLogGeneral().getDateTrn(), UUID.randomUUID().toString(), context.getHttpServerRequest().absoluteURI(), ResponseCode.OK_RESPONSE.getMessage(), ResponseCode.OK_RESPONSE.getMessage(), ctlObject);
        }
        return result;
    }
}
