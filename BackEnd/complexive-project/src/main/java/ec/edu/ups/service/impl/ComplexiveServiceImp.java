package ec.edu.ups.service.impl;

import com.google.gson.GsonBuilder;
import ec.edu.ups.logic.*;
import ec.edu.ups.model.ProjectPlotIn;
import ec.edu.ups.model.ProjectPlotOut;
import ec.edu.ups.service.IComplexiveService;
import ec.edu.ups.util.AccessControlFilter;
import org.jboss.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.text.ParseException;

@ApplicationScoped
public class ComplexiveServiceImp implements IComplexiveService {
    private final Logger log = Logger.getLogger(ComplexiveServiceImp.class);
    @Inject
    PersonProccess personProccess;
    @Inject
    NacionalityProccess nacionalityProccess;
    @Inject
    EducationLevelProccess educationLevelProccess;
    @Inject
    CountryProcess countryProcess;
    @Inject
    AverageProccess averageProccess;

    @Override
    public Response mantenaincePersonBasic(AccessControlFilter context, ProjectPlotIn plot) throws ParseException {
        log.info("ENDPOINT DE MANTENIMIENTO PERSONAS" + "\nRequest:" + new GsonBuilder().disableHtmlEscaping().serializeNulls().create().toJson(plot));
        ProjectPlotOut result = personProccess.maintenancePerson(context, plot);
        log.info("Endpoint: " + context.getHttpServerRequest().absoluteURI() + "\nRespuesta " + new GsonBuilder().serializeNulls().disableHtmlEscaping().create().toJson(result));
        return Response.ok(result).status(Integer.parseInt(result.getCode())).build();
    }

    @Override
    public Response getPersonBasic(AccessControlFilter context, ProjectPlotIn plot) {
        log.info("ENDPOINT DE GET PERSONAS" + "\nRequest:" + new GsonBuilder().disableHtmlEscaping().serializeNulls().create().toJson(plot));
        ProjectPlotOut result = personProccess.getDataPerson(context, plot);
        log.info("Endpoint: " + context.getHttpServerRequest().absoluteURI() + "\nRespuesta " + new GsonBuilder().serializeNulls().disableHtmlEscaping().create().toJson(result));
        return Response.ok(result).status(Integer.parseInt(result.getCode())).build();
    }

    @Override
    public Response mantenainceNacionality(AccessControlFilter context, ProjectPlotIn plot) {
        log.info("ENDPOINT DE MANTENIMIENTO NACIONALIDADES" + "\nRequest:" + new GsonBuilder().disableHtmlEscaping().serializeNulls().create().toJson(plot));
        ProjectPlotOut result = nacionalityProccess.maintenaceNacionality(context, plot);
        log.info("Endpoint: " + context.getHttpServerRequest().absoluteURI() + "\nRespuesta " + new GsonBuilder().serializeNulls().disableHtmlEscaping().create().toJson(result));
        return Response.ok(result).status(Integer.parseInt(result.getCode())).build();
    }

    @Override
    public Response getNacionalityBasic(AccessControlFilter context, ProjectPlotIn plot) {
        log.info("ENDPOINT DE GET NACIONALIDADES" + "\nRequest:" + new GsonBuilder().disableHtmlEscaping().serializeNulls().create().toJson(plot));
        ProjectPlotOut result = nacionalityProccess.getNacionality(context, plot);
        log.info("Endpoint: " + context.getHttpServerRequest().absoluteURI() + "\nRespuesta " + new GsonBuilder().serializeNulls().disableHtmlEscaping().create().toJson(result));
        return Response.ok(result).status(Integer.parseInt(result.getCode())).build();
    }

    @Override
    public Response getEducationLevel(AccessControlFilter context, ProjectPlotIn plot) {
        log.info("ENDPOINT DE GET NIVEL EDUCACION" + "\nRequest:" + new GsonBuilder().disableHtmlEscaping().serializeNulls().create().toJson(plot));
        ProjectPlotOut result = educationLevelProccess.getEducationLevel(context, plot);
        log.info("Endpoint: " + context.getHttpServerRequest().absoluteURI() + "\nRespuesta " + new GsonBuilder().serializeNulls().disableHtmlEscaping().create().toJson(result));
        return Response.ok(result).status(Integer.parseInt(result.getCode())).build();
    }

    @Override
    public Response getCountries(AccessControlFilter context, ProjectPlotIn plot) throws IOException, InterruptedException {
        log.info("ENDPOINT DE GET PAISES" + "\nRequest:" + new GsonBuilder().disableHtmlEscaping().serializeNulls().create().toJson(plot));
        ProjectPlotOut result = countryProcess.getCountries(context, plot);
        log.info("Endpoint: " + context.getHttpServerRequest().absoluteURI() + "\nRespuesta " + new GsonBuilder().serializeNulls().disableHtmlEscaping().create().toJson(result));
        return Response.ok(result).status(Integer.parseInt(result.getCode())).build();
    }

    @Override
    public Response getAverage(AccessControlFilter context, ProjectPlotIn plot) {
        log.info("ENDPOINT DE CALCULAR PROMEDIO" + "\nRequest:" + new GsonBuilder().disableHtmlEscaping().serializeNulls().create().toJson(plot));
        ProjectPlotOut result = averageProccess.getAveragePerNacionality(context, plot);
        log.info("Endpoint: " + context.getHttpServerRequest().absoluteURI() + "\nRespuesta " + new GsonBuilder().serializeNulls().disableHtmlEscaping().create().toJson(result));
        return Response.ok(result).status(Integer.parseInt(result.getCode())).build();
    }
}
