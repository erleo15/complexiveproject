package ec.edu.ups.logic;

import com.google.gson.GsonBuilder;
import ec.edu.ups.logging.LogGeneral;
import ec.edu.ups.model.*;
import ec.edu.ups.util.AccessControlFilter;
import ec.edu.ups.util.ConsumeREST;
import ec.edu.ups.util.ResponseCode;
import ec.edu.ups.util.Tools;

import javax.enterprise.context.ApplicationScoped;
import java.io.IOException;
import java.util.*;

@ApplicationScoped
public class CountryProcess {
    public ProjectPlotOut getCountries(AccessControlFilter context, ProjectPlotIn plot) throws IOException, InterruptedException {
        List<Country> listaPaises = new ArrayList<>();
        ProjectPlotOut result = new ProjectPlotOut("404", plot.getUserApp(), context.getLogGeneral().getDateTrn(), UUID.randomUUID().toString(), context.getHttpServerRequest().absoluteURI(), "REGISTRO NO ENCONTRADO", "REGISTRO NO ENCONTRADO", null);
        HttpConsume httpConsume = new HttpConsume(new LogGeneral(), "https://raw.githubusercontent.com/millan2993/countries/master/json/countries.json", null);
        String resultado = ConsumeREST.consumeGETWS(httpConsume).body().trim();
        ApiPais apiPais = new GsonBuilder().serializeNulls().disableHtmlEscaping().create().fromJson(resultado, ApiPais.class);
        Collections.sort(apiPais.getCountries());
        if (Tools.verify(plot, "cPais")) {
            try {
                Country resultFind = apiPais.getCountries().stream()
                        .filter(country -> Integer.parseInt(plot.getCtl().get("cPais").toString()) == country.getId())
                        .findAny()
                        .orElse(null);
                listaPaises.add(resultFind);
            } catch (NumberFormatException e) {
                result = new ProjectPlotOut(ResponseCode.ERROR_NUMBERFORMAT_EX.getResponseCode(), plot.getUserApp(), context.getLogGeneral().getDateTrn(), UUID.randomUUID().toString(), context.getHttpServerRequest().absoluteURI(), ResponseCode.ERROR_NUMBERFORMAT_EX.getMessage(), ResponseCode.ERROR_NUMBERFORMAT_EX.getMessage(), null);
            }
        } else {
            listaPaises = apiPais.getCountries();
            Collections.sort(listaPaises);
        }
        Map<String, Object> ctlObject = new LinkedHashMap<>();
        ctlObject.put("paises", listaPaises);
        if (listaPaises.get(0) != null || !listaPaises.isEmpty()) {
            result = new ProjectPlotOut(ResponseCode.ERROR_NOT_FOUND.getResponseCode(), plot.getUserApp(), context.getLogGeneral().getDateTrn(), UUID.randomUUID().toString(), context.getHttpServerRequest().absoluteURI(), ResponseCode.ERROR_NOT_FOUND.getMessage(), ResponseCode.ERROR_NOT_FOUND.getMessage(), null);
        } else {
            result = new ProjectPlotOut(ResponseCode.OK_RESPONSE.getResponseCode(), plot.getUserApp(), context.getLogGeneral().getDateTrn(), UUID.randomUUID().toString(), context.getHttpServerRequest().absoluteURI(), ResponseCode.OK_RESPONSE.getMessage(), ResponseCode.OK_RESPONSE.getMessage(), ctlObject);
        }
        return result;
    }
}
