package ec.edu.ups.logic;

import ec.edu.ups.model.*;
import ec.edu.ups.util.AccessControlFilter;
import ec.edu.ups.util.ResponseCode;
import ec.edu.ups.util.Tools;

import javax.enterprise.context.ApplicationScoped;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@ApplicationScoped
public class PersonProccess {
    public ProjectPlotOut maintenancePerson(AccessControlFilter context, ProjectPlotIn plot) throws ParseException {
        ProjectPlotOut result = new ProjectPlotOut(ResponseCode.ERROR_INTERNAL_SERVER.getResponseCode(), plot.getUserApp(), context.getLogGeneral().getDateTrn(), UUID.randomUUID().toString(), context.getHttpServerRequest().absoluteURI(), ResponseCode.ERROR_INTERNAL_SERVER.getMessage(), ResponseCode.ERROR_INTERNAL_SERVER.getMessage(), null);
        SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
        Date dataFormateada = formato.parse(plot.getCtl().get("fechaNacimiento").toString());
        if (!Tools.verify(plot, "codigo")) {
            List<Nacionalidad> listaNacionalidades = Nacionalidad.findAll().list();
            Nacionalidad nacionalidadEncontrada = listaNacionalidades.stream()
                    .filter(nacionalidad -> plot.getCtl().get("cNacionalidad").toString().compareToIgnoreCase(nacionalidad.getISONacionalidad()) == 0)
                    .findAny()
                    .orElse(null);
            List<NivelEducacion> listaNivelEducacion = NivelEducacion.findAll().list();
            NivelEducacion nivelEducacionEncontrada = listaNivelEducacion.stream()
                    .filter(nivelEducacion -> plot.getCtl().get("cNivelEducacion").toString().compareToIgnoreCase(nivelEducacion.getSigla()) == 0)
                    .findAny()
                    .orElse(null);
            Persona persona = new Persona(
                    plot.getCtl().get("primerNombre").toString(),
                    plot.getCtl().get("segundoNombre").toString(),
                    plot.getCtl().get("apellidoPaterno").toString(),
                    plot.getCtl().get("apellidoMaterno").toString(),
                    dataFormateada, plot.getCtl().get("sexo").toString(),
                    nivelEducacionEncontrada,
                    nacionalidadEncontrada
            );
            persona.persist();
            List<Persona> listaPersonas = new ArrayList<>();
            listaPersonas.add(persona);
            Map<String, Object> ctlObject = new LinkedHashMap<>();
            ctlObject.put("persona", listaPersonas);
            result = new ProjectPlotOut(ResponseCode.OK_RESPONSE.getResponseCode(), plot.getUserApp(), context.getLogGeneral().getDateTrn(), UUID.randomUUID().toString(), context.getHttpServerRequest().absoluteURI(), "TRANSACCION REALIZADA CORRECTAMENTE", "TRANSACCION REALIZADA CORRECTAMENTE", ctlObject);
        } else {
            Persona persona = new Persona(
                    Integer.parseInt(plot.getCtl().get("codigo").toString()),
                    plot.getCtl().get("primerNombre").toString(),
                    plot.getCtl().get("segundoNombre").toString(),
                    plot.getCtl().get("apellidoPaterno").toString(),
                    plot.getCtl().get("apellidoMaterno").toString(),
                    dataFormateada, plot.getCtl().get("sexo").toString(),
                    NivelEducacion.findById(Integer.parseInt(plot.getCtl().get("cNivelEducacion").toString())),
                    Nacionalidad.findById(Integer.parseInt(plot.getCtl().get("cNacionalidad").toString())));
            List<Persona> listaPersonas = new ArrayList<>();
            listaPersonas.add(persona);
            Map<String, Object> ctlObject = new LinkedHashMap<>();
            ctlObject.put("persona", listaPersonas);
            result = new ProjectPlotOut(ResponseCode.OK_RESPONSE.getResponseCode(), plot.getUserApp(), context.getLogGeneral().getDateTrn(), UUID.randomUUID().toString(), context.getHttpServerRequest().absoluteURI(), "TRANSACCION REALIZADA CORRECTAMENTE", "TRANSACCION REALIZADA CORRECTAMENTE", ctlObject);
        }
        return result;
    }

    public ProjectPlotOut getDataPerson(AccessControlFilter context, ProjectPlotIn plot) {
        List<Persona> listaPersonas = new ArrayList<>();
        ProjectPlotOut result = new ProjectPlotOut(ResponseCode.ERROR_NOT_FOUND.getResponseCode(), plot.getUserApp(), context.getLogGeneral().getDateTrn(), UUID.randomUUID().toString(), context.getHttpServerRequest().absoluteURI(), ResponseCode.ERROR_NOT_FOUND.getMessage(), ResponseCode.ERROR_NOT_FOUND.getMessage(), null);
        if (Tools.verify(plot, "cPersona")) {
            try {
                listaPersonas.add(Persona.findById(Integer.parseInt(plot.getCtl().get("cPersona").toString())));
            } catch (NumberFormatException e) {
                result = new ProjectPlotOut(ResponseCode.ERROR_NUMBERFORMAT_EX.getResponseCode(), plot.getUserApp(), context.getLogGeneral().getDateTrn(), UUID.randomUUID().toString(), context.getHttpServerRequest().absoluteURI(), ResponseCode.ERROR_NUMBERFORMAT_EX.getMessage(), ResponseCode.ERROR_NUMBERFORMAT_EX.getMessage(), null);
            }
        } else {
            listaPersonas = Persona.findAll().list();
        }
        Map<String, Object> ctlObject = new LinkedHashMap<>();
        ctlObject.put("persona", listaPersonas);
        if (!(listaPersonas.isEmpty())) {
            result = new ProjectPlotOut(ResponseCode.OK_RESPONSE.getResponseCode(), plot.getUserApp(), context.getLogGeneral().getDateTrn(), UUID.randomUUID().toString(), context.getHttpServerRequest().absoluteURI(), ResponseCode.OK_RESPONSE.getMessage(), ResponseCode.OK_RESPONSE.getMessage(), ctlObject);
        }
        return result;
    }
}
