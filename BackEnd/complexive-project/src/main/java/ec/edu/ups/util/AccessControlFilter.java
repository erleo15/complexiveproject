package ec.edu.ups.util;

import ec.edu.ups.logging.LogGeneral;
import io.vertx.core.http.HttpServerRequest;
import lombok.Data;
import org.jboss.logging.Logger;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Context;
import javax.ws.rs.ext.Provider;

/*
 * @(#) AccessControlFilter.java 1.0 16/12/2022
 * Copyright 2022 BA, Inc. Todos los derechos reservados.
 * SUN PROPIETARY/CONFIDENTIAL
 */

/**
 * Clase para filtrar las consultas antes de llegar al endpoint.
 *
 * @author Banco del Austro S. A.
 * @version 1.0, 16/12/2022
 * @desde 1.0
 */
@Provider
@Data
public class AccessControlFilter implements ContainerRequestFilter {
    private static final Logger log = Logger.getLogger(AccessControlFilter.class);
    @Context
    HttpServerRequest httpServerRequest;
    private LogGeneral logGeneral;

    /**
     * Método para filtrar los request.
     *
     * @param requestContext Contexto de request.
     */
    @Override
    public void filter(ContainerRequestContext requestContext) {
        logGeneral = new LogGeneral();
        log.info(String.format(StaticContent.LOG_FORMATER, logGeneral.getUUIdMicroservicio(), "************************-START FLOW-*******************"));
        log.info(String.format(StaticContent.LOG_FORMATER, logGeneral.getUUIdMicroservicio(), "CALLED ENDPOINT: " + httpServerRequest.absoluteURI()));
    }
}