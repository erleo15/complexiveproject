package ec.edu.ups.logic;

import ec.edu.ups.model.Nacionalidad;
import ec.edu.ups.model.Persona;
import ec.edu.ups.model.ProjectPlotIn;
import ec.edu.ups.model.ProjectPlotOut;
import ec.edu.ups.util.AccessControlFilter;
import ec.edu.ups.util.ResponseCode;
import ec.edu.ups.util.Tools;

import javax.enterprise.context.ApplicationScoped;
import java.util.*;
import java.util.stream.Collectors;

@ApplicationScoped
public class AverageProccess {
    public ProjectPlotOut getAveragePerNacionality(AccessControlFilter context, ProjectPlotIn plot) {
        ProjectPlotOut result = new ProjectPlotOut("404", plot.getUserApp(), context.getLogGeneral().getDateTrn(), UUID.randomUUID().toString(), context.getHttpServerRequest().absoluteURI(), "REGISTRO NO ENCONTRADO", "REGISTRO NO ENCONTRADO", null);
        List<Persona> listaPersonasOriginal = Persona.findAll().list();

        List<Nacionalidad> listaNacionalidades = Nacionalidad.findAll().list();
        if (Tools.verify(plot, "cNacionalidad")) {
            result = getAverageSpecificNacionality(plot, listaPersonasOriginal, result, context);
        } else {
            result = getAverageAllNacionality(listaNacionalidades, listaPersonasOriginal, result, plot, context);
        }
        return result;
    }

    ProjectPlotOut getAverageSpecificNacionality(ProjectPlotIn plot, List<Persona> listaPersonasOriginal, ProjectPlotOut resultDefault, AccessControlFilter context) {
        Map<String, Object> ctlObject = new LinkedHashMap<>();
        try {
            int codigoNacionalidad = Integer.parseInt(plot.getCtl().get("cNacionalidad").toString());
            Nacionalidad nacionalidad = Nacionalidad.findById(codigoNacionalidad);
            List<Persona> listaPersonas = listaPersonasOriginal
                    .stream()
                    .filter(persona -> persona.getNacionalidad().getCodigo() == codigoNacionalidad)
                    .collect(Collectors.toList());
            if (listaPersonas.size() > 0) {
                ctlObject.put(nacionalidad.getGenticilioNacionalidad(), Tools.calculateAccurrate(listaPersonas));
                List<Object> listaPromedios = new ArrayList<>();
                listaPromedios.add(ctlObject);
                ctlObject = new HashMap<>();
                ctlObject.put("edadesPromedio", listaPromedios);
                return new ProjectPlotOut(ResponseCode.OK_RESPONSE.getResponseCode(), plot.getUserApp(), context.getLogGeneral().getDateTrn(), UUID.randomUUID().toString(), context.getHttpServerRequest().absoluteURI(), ResponseCode.OK_RESPONSE.getMessage(), ResponseCode.OK_RESPONSE.getMessage(), ctlObject);
            }
        } catch (NumberFormatException e) {
            return new ProjectPlotOut(ResponseCode.ERROR_NUMBERFORMAT_EX.getResponseCode(), plot.getUserApp(), context.getLogGeneral().getDateTrn(), UUID.randomUUID().toString(), context.getHttpServerRequest().absoluteURI(), ResponseCode.ERROR_NUMBERFORMAT_EX.getMessage(), ResponseCode.ERROR_NUMBERFORMAT_EX.getMessage(), null);
        }
        return resultDefault;
    }

    ProjectPlotOut getAverageAllNacionality(List<Nacionalidad> listaNacionalidades, List<Persona> listaPersonasOriginal, ProjectPlotOut resultDefault, ProjectPlotIn plot, AccessControlFilter context) {
        Map<String, Object> ctlObject = new LinkedHashMap<>();
        List<Object> listaPromedios = new ArrayList<>();
        for (Nacionalidad nacionalidad : listaNacionalidades) {
            List<Persona> listaPersonas = listaPersonasOriginal
                    .stream()
                    .filter(persona -> persona.getNacionalidad().getCodigo() == nacionalidad.getCodigo())
                    .collect(Collectors.toList());
            if (listaPersonas.size() > 0) {
                ctlObject.put(nacionalidad.getGenticilioNacionalidad(), Tools.calculateAccurrate(listaPersonas));

            }
        }
        if (ctlObject.size() > 0) {
            listaPromedios.add(ctlObject);
            ctlObject = new HashMap<>();
            ctlObject.put("edadesPromedio", listaPromedios);
            resultDefault = new ProjectPlotOut(ResponseCode.OK_RESPONSE.getResponseCode(), plot.getUserApp(), context.getLogGeneral().getDateTrn(), UUID.randomUUID().toString(), context.getHttpServerRequest().absoluteURI(), ResponseCode.OK_RESPONSE.getMessage(), ResponseCode.OK_RESPONSE.getMessage(), ctlObject);
        }
        return resultDefault;
    }
}
