package ec.edu.ups.model;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "persona")
public class Persona extends PanacheEntityBase {
    @Id
    @Column(name = "PER_CODIGO")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int codigo;
    @Column(name = "PER_PRIMER_NOMBRE")
    private String primerNombre;
    @Column(name = "PER_SEGUNDO_NOMBRE")
    private String segundoNombre;
    @Column(name = "PER_APELLIDO_PATERNO")
    private String apellidoPaterno;
    @Column(name = "PER_APELLIDO_MATERNO")
    private String apellidoMaterno;
    @Column(name = "PER_FECHA_NACIMIENTO")
    private Date fechaNacimiento;
    @Column(name = "PER_SEXO")
    private String sexo;
    @JoinColumn(name = "PER_NE_CODIGO", nullable = false)
    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private NivelEducacion nivelEducacion;
    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "PER_NAC_CODIGO", nullable = false)
    private Nacionalidad nacionalidad;

    public Persona(String primerNombre, String segundoNombre, String apellidoPaterno, String apellidoMaterno, Date fechaNacimiento, String sexo, NivelEducacion nivelEducacion, Nacionalidad nacionalidad) {
        this.primerNombre = primerNombre;
        this.segundoNombre = segundoNombre;
        this.apellidoPaterno = apellidoPaterno;
        this.apellidoMaterno = apellidoMaterno;
        this.fechaNacimiento = fechaNacimiento;
        this.sexo = sexo;
        this.nivelEducacion = nivelEducacion;
        this.nacionalidad = nacionalidad;
    }
}
