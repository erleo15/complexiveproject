package ec.edu.ups.controller;

import ec.edu.ups.model.ProjectPlotIn;
import ec.edu.ups.service.impl.ComplexiveServiceImp;
import ec.edu.ups.util.AccessControlFilter;
import io.netty.util.internal.StringUtil;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.text.ParseException;

@ApplicationScoped
@Path(StringUtil.EMPTY_STRING)
public class GeneralController {
    @Inject
    public ComplexiveServiceImp complexiveServiceImp;

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Transactional
    @Path("man/person/basic")
    public Response mantenaincePersonBasic(@Context AccessControlFilter context, ProjectPlotIn plot) throws ParseException {
        return complexiveServiceImp.mantenaincePersonBasic(context, plot);
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Transactional
    @Path("con/person/basic")
    public Response getPersonBasic(@Context AccessControlFilter context, ProjectPlotIn plot) {
        return complexiveServiceImp.getPersonBasic(context, plot);
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Transactional
    @Path("man/nacionality/basic")
    public Response mantenainceNacionality(@Context AccessControlFilter context, ProjectPlotIn plot) {
        return complexiveServiceImp.mantenainceNacionality(context, plot);
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Transactional
    @Path("con/nacionality/basic")
    public Response getNacionalityBasic(@Context AccessControlFilter context, ProjectPlotIn plot) {
        return complexiveServiceImp.getNacionalityBasic(context, plot);
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Transactional
    @Path("con/education/level/basic")
    public Response getEducationLevelBasic(@Context AccessControlFilter context, ProjectPlotIn plot) {
        return complexiveServiceImp.getEducationLevel(context, plot);
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Transactional
    @Path("con/countries")
    public Response getCountries(@Context AccessControlFilter context, ProjectPlotIn plot) throws IOException, InterruptedException {
        return complexiveServiceImp.getCountries(context, plot);
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Transactional
    @Path("con/average")
    public Response getAverage(@Context AccessControlFilter context, ProjectPlotIn plot) {
        return complexiveServiceImp.getAverage(context, plot);
    }
}