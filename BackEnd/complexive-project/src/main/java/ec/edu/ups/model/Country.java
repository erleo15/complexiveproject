package ec.edu.ups.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Country implements Comparable<Country> {
    private int id;
    private String name;

    @Override
    public int compareTo(Country country) {
        return this.id - country.getId();
    }
}

