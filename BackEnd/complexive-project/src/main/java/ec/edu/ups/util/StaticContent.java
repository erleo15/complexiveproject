package ec.edu.ups.util;
/*
 * @(#) StaticContent.java 1.0 16/12/2022
 * Copyright 2022 BA, Inc. Todos los derechos reservados.
 * SUN PROPIETARY/CONFIDENTIAL
 */

/**
 * Clase para manejar variables constantes.
 *
 * @author Banco del Austro S. A.
 * @version 1.0, 16/12/2022
 * @desde 1.0
 */
public class StaticContent {
    public static final String LOG_FORMATER_SHORT = "%s ";
    public static final String LOG_FORMATER = "%s - %s ";
    public static final String LOG_FORMATER_LARGE = "%s - %s - %s ";
    public static final String STR_FORMAT_TIMESTAMP_DATE = "yyyy-MM-dd HH:mm:ss";
    public static final String STR_FORMAT_SLASH_DD_MM_YYYY = "dd/MM/yyyy";

    private StaticContent() {
        throw new IllegalStateException("Utility class");
    }

}
