package ec.edu.ups.util;

import ec.edu.ups.model.Persona;
import ec.edu.ups.model.ProjectPlotIn;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

public class Tools {
    public static boolean verify(ProjectPlotIn plot, String... params) {
        for (String param : params) {
            if (!plot.getCtl().containsKey(param)) {
                return false;
            }
            if (plot.getCtl().get(param) == null) {
                return false;
            }
            if (plot.getCtl().get(param).toString().isEmpty()) {
                return false;
            }
        }
        return true;
    }

    public static String calculateAccurrate(List<Persona> listaPersonas) {
        int edadNum = 0;
        for (Persona persona : listaPersonas) {
            String edad = getEdad(persona.getFechaNacimiento());
            edadNum += Integer.parseInt(edad);
        }
        double edadProm = Double.parseDouble(String.valueOf(edadNum)) / Double.parseDouble(String.valueOf(listaPersonas.size()));
        return String.format("%.2f", edadProm);
    }

    public static String getEdad(Date fechaNacimiento) {
        if (fechaNacimiento != null) {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            StringBuilder result = new StringBuilder();
            Calendar c = new GregorianCalendar();
            c.setTime(fechaNacimiento);
            result.append(calcularEdad(c));
            return result.toString();
        }
        return "";
    }

    private static int calcularEdad(Calendar fechaNac) {
        Calendar today = Calendar.getInstance();
        int diffYear = today.get(Calendar.YEAR) - fechaNac.get(Calendar.YEAR);
        int diffMonth = today.get(Calendar.MONTH) - fechaNac.get(Calendar.MONTH);
        int diffDay = today.get(Calendar.DAY_OF_MONTH) - fechaNac.get(Calendar.DAY_OF_MONTH);
        if (diffMonth < 0 || (diffMonth == 0 && diffDay < 0)) {
            diffYear = diffYear - 1;
        }
        return diffYear;
    }
}
