package ec.edu.ups.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ProjectPlotOut {
    private String code;
    private String usr;
    private String dateTrn;
    private String uuidTrn;
    private String wsRef;
    private String msgUser;
    private String msgTech;
    private Map<String, Object> ctl;
}
