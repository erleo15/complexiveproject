package ec.edu.ups.model;

import ec.edu.ups.logging.LogGeneral;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class HttpConsume {
    private LogGeneral logGeneral;
    private String endpoint;
    private String body;
    private Map<String, String> headers;

    /**
     * Método constructor para incializar los parámetros con valor y declarar nueva instancia de headers.
     *
     * @param logGeneral Parametro de log.
     * @param endpoint   Parámetro que indica el endpoint a usar en el consumo.
     * @param body       Parámetro de body del servicio.
     */
    public HttpConsume(LogGeneral logGeneral, String endpoint, String body) {
        this.logGeneral = logGeneral;
        this.endpoint = endpoint;
        this.body = body;
        headers = new HashMap<>();
    }
}
