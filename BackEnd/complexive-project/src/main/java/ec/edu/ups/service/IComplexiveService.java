package ec.edu.ups.service;

import ec.edu.ups.model.ProjectPlotIn;
import ec.edu.ups.util.AccessControlFilter;

import javax.ws.rs.core.Response;
import java.io.IOException;
import java.text.ParseException;

public interface IComplexiveService {
    Response mantenaincePersonBasic(AccessControlFilter context, ProjectPlotIn plot) throws ParseException;
    Response getPersonBasic(AccessControlFilter context, ProjectPlotIn plot);
    Response mantenainceNacionality(AccessControlFilter context, ProjectPlotIn plot);
    Response getNacionalityBasic(AccessControlFilter context, ProjectPlotIn plot);
    Response getEducationLevel(AccessControlFilter context, ProjectPlotIn plot);
    Response getCountries(AccessControlFilter context, ProjectPlotIn plot) throws IOException, InterruptedException;
    Response getAverage(AccessControlFilter context, ProjectPlotIn plot);

}
