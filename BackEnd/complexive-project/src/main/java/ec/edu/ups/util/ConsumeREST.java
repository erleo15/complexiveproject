package ec.edu.ups.util;

import ec.edu.ups.model.HttpConsume;
import org.jboss.logging.Logger;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class ConsumeREST {
    static final Logger log = Logger.getLogger(ConsumeREST.class);

    private ConsumeREST() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * Método para consumir un servicio POST.
     *
     * @param httpConsume Objeto modelo donde están declarados los parametros del servicio a consumir.
     * @return Objeto HttpResponse con el resultado del servicio consumido.
     * @throws IOException          Error en el consumo.
     * @throws InterruptedException Error de interrupción en el consumo.
     */
    public static HttpResponse<String> consumePOSTWS(HttpConsume httpConsume) throws IOException, InterruptedException {
        log.info(String.format(StaticContent.LOG_FORMATER, httpConsume.getLogGeneral().getGuid(), "SE CONSUME EL ENDPOINT: " + httpConsume.getEndpoint() + " || HEADERS: " + httpConsume.getHeaders() + " || BODY: " + httpConsume.getBody()));
        HttpRequest.Builder request = HttpRequest.newBuilder(URI.create(httpConsume.getEndpoint())).POST(HttpRequest.BodyPublishers.ofString(httpConsume.getBody()));
        for (String key : httpConsume.getHeaders().keySet()) {
            String value = httpConsume.getHeaders().get(key);
            request.setHeader(key, value);
        }
        HttpClient httpClient = HttpClient.newBuilder().version(HttpClient.Version.HTTP_2).build();
        long tInicial = System.currentTimeMillis();
        HttpResponse<String> response = httpClient.send(request.build(), HttpResponse.BodyHandlers.ofString());
        long tFin = System.currentTimeMillis();
        log.info(String.format(StaticContent.LOG_FORMATER, httpConsume.getLogGeneral().getGuid(), "SERVICIO " + httpConsume.getEndpoint() + " CONSUMIDO EN " + (tFin - tInicial) + "ms." + " Result: " + response.body()));
        return response;
    }

    /**
     * Método para consumir un servicio GET.
     *
     * @param httpConsume Objeto modelo donde están declarados los parámetros del servicio a consumir.
     * @return Objeto HttpResponse con el resultado del servicio consumido.
     * @throws IOException          Error en el consumo.
     * @throws InterruptedException Error de interrupcion en el consumo.
     */
    public static HttpResponse<String> consumeGETWS(HttpConsume httpConsume) throws IOException, InterruptedException {
        log.info(String.format(StaticContent.LOG_FORMATER, httpConsume.getLogGeneral().getGuid(), "SE CONSUME EL ENDPOINT: " + httpConsume.getEndpoint() + " || HEADERS: " + httpConsume.getHeaders().toString()) + " || BODY: " + httpConsume.getBody());
        HttpRequest.Builder request = HttpRequest.newBuilder(URI.create(httpConsume.getEndpoint())).GET();
        for (String key : httpConsume.getHeaders().keySet()) {
            String value = httpConsume.getHeaders().get(key);
            request.setHeader(key, value);
        }
        HttpClient httpClient = HttpClient.newBuilder().version(HttpClient.Version.HTTP_2).build();
        long tInicial = System.currentTimeMillis();
        HttpResponse<String> response = httpClient.send(request.build(), HttpResponse.BodyHandlers.ofString());
        long tFin = System.currentTimeMillis();
        log.info(String.format(StaticContent.LOG_FORMATER, httpConsume.getLogGeneral().getGuid(), "SERVICIO " + httpConsume.getEndpoint() + " CONSUMIDO EN " + (tFin - tInicial) + "ms." + " Result: " + response.body()));
        return response;
    }
}
