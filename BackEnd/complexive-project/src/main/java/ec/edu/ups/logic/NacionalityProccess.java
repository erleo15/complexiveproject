package ec.edu.ups.logic;

import ec.edu.ups.model.Nacionalidad;
import ec.edu.ups.model.Persona;
import ec.edu.ups.model.ProjectPlotIn;
import ec.edu.ups.model.ProjectPlotOut;
import ec.edu.ups.util.AccessControlFilter;
import ec.edu.ups.util.ResponseCode;
import ec.edu.ups.util.Tools;

import javax.enterprise.context.ApplicationScoped;
import java.util.*;

@ApplicationScoped
public class NacionalityProccess {
    public ProjectPlotOut getNacionality(AccessControlFilter context, ProjectPlotIn plot) {
        List<Nacionalidad> listaNacionalidades = new ArrayList<>();
        ProjectPlotOut result = new ProjectPlotOut(ResponseCode.ERROR_NOT_FOUND.getResponseCode(), plot.getUserApp(), context.getLogGeneral().getDateTrn(), UUID.randomUUID().toString(), context.getHttpServerRequest().absoluteURI(), "REGISTRO NO ENCONTRADO", "REGISTRO NO ENCONTRADO", null);
        if (Tools.verify(plot, "cNacionalidad")) {
            try {
                listaNacionalidades.add(Nacionalidad.findById(Integer.parseInt(plot.getCtl().get("cNacionalidad").toString())));
            } catch (NumberFormatException e) {
                result = new ProjectPlotOut(ResponseCode.ERROR_NUMBERFORMAT_EX.getResponseCode(), plot.getUserApp(), context.getLogGeneral().getDateTrn(), UUID.randomUUID().toString(), context.getHttpServerRequest().absoluteURI(), ResponseCode.ERROR_NUMBERFORMAT_EX.getMessage(), ResponseCode.ERROR_NUMBERFORMAT_EX.getMessage(), null);
            }
        } else {
            listaNacionalidades = Nacionalidad.findAll().list();
        }
        Map<String, Object> ctlObject = new LinkedHashMap<>();
        ctlObject.put("nacionalidad", listaNacionalidades);
        if (listaNacionalidades.get(0) == null || listaNacionalidades.isEmpty()) {
            result = new ProjectPlotOut(ResponseCode.ERROR_NOT_FOUND.getResponseCode(), plot.getUserApp(), context.getLogGeneral().getDateTrn(), UUID.randomUUID().toString(), context.getHttpServerRequest().absoluteURI(), ResponseCode.ERROR_NOT_FOUND.getMessage(), ResponseCode.ERROR_NOT_FOUND.getMessage(), null);
        } else {
            result = new ProjectPlotOut(ResponseCode.OK_RESPONSE.getResponseCode(), plot.getUserApp(), context.getLogGeneral().getDateTrn(), UUID.randomUUID().toString(), context.getHttpServerRequest().absoluteURI(), ResponseCode.OK_RESPONSE.getMessage(), ResponseCode.OK_RESPONSE.getMessage(), ctlObject);
        }
        return result;
    }

    public ProjectPlotOut maintenaceNacionality(AccessControlFilter context, ProjectPlotIn plot) {
        ProjectPlotOut result = new ProjectPlotOut(ResponseCode.ERROR_INTERNAL_SERVER.getResponseCode(), plot.getUserApp(), context.getLogGeneral().getDateTrn(), UUID.randomUUID().toString(), context.getHttpServerRequest().absoluteURI(), ResponseCode.OK_RESPONSE.getMessage(), ResponseCode.OK_RESPONSE.getMessage(), null);
        if (Tools.verify(plot, "codigo")) {
            Nacionalidad nacionalidad = new Nacionalidad(
                    plot.getCtl().get("paisNacionalidad").toString(),
                    plot.getCtl().get("genticilioNacionalidad").toString(),
                    plot.getCtl().get("ISONacionalidad").toString()
            );
            Nacionalidad.persist(nacionalidad);
            List<Nacionalidad> listaPersonas = new ArrayList<>();
            listaPersonas.add(nacionalidad);
            Map<String, Object> ctlObject = new LinkedHashMap<>();
            ctlObject.put("nacionalidad", listaPersonas);
            result = new ProjectPlotOut(ResponseCode.OK_RESPONSE.getResponseCode(), plot.getUserApp(), context.getLogGeneral().getDateTrn(), UUID.randomUUID().toString(), context.getHttpServerRequest().absoluteURI(), ResponseCode.OK_RESPONSE.getMessage(), ResponseCode.OK_RESPONSE.getMessage(), ctlObject);
        } else {
            Nacionalidad nacionalidad = new Nacionalidad(
                    Integer.parseInt(plot.getCtl().get("codigo").toString()),
                    plot.getCtl().get("paisNacionalidad").toString(),
                    plot.getCtl().get("genticilioNacionalidad").toString(),
                    plot.getCtl().get("ISONacionalidad").toString());
            Persona.getEntityManager().merge(nacionalidad);
            List<Nacionalidad> listaPersonas = new ArrayList<>();
            listaPersonas.add(nacionalidad);
            Map<String, Object> ctlObject = new LinkedHashMap<>();
            ctlObject.put("nacionalidad", listaPersonas);
            result = new ProjectPlotOut(ResponseCode.OK_RESPONSE.getResponseCode(), plot.getUserApp(), context.getLogGeneral().getDateTrn(), UUID.randomUUID().toString(), context.getHttpServerRequest().absoluteURI(), ResponseCode.OK_RESPONSE.getMessage(), ResponseCode.OK_RESPONSE.getMessage(), ctlObject);
        }
        return result;
    }


}
