package ec.edu.ups.model;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "nacionalidad")
public class Nacionalidad extends PanacheEntityBase {
    @Id
    @Column(name = "NAC_CODIGO")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int codigo;
    @Column(name = "NAC_PAIS_NAC")
    private String paisNacionalidad;
    @Column(name = "NAC_GENTILICIO_NAC")
    private String genticilioNacionalidad;
    @Column(name = "NAC_ISO_NAC")
    private String ISONacionalidad;

    public Nacionalidad(String paisNacionalidad, String genticilioNacionalidad, String ISONacionalidad) {
        this.paisNacionalidad = paisNacionalidad;
        this.genticilioNacionalidad = genticilioNacionalidad;
        this.ISONacionalidad = ISONacionalidad;
    }
}
