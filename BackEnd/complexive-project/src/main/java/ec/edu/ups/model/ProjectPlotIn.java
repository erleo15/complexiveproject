package ec.edu.ups.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ProjectPlotIn {
    private String ipUser;
    private String uuidUser;
    private String userApp;
    private String channel;
    private String operation;
    private Map<String, Object> ctl;
}
