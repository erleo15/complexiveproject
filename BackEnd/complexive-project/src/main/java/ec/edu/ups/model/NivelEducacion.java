package ec.edu.ups.model;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "nivel_educacion")
public class NivelEducacion extends PanacheEntityBase {
    @Id
    @Column(name = "NE_CODIGO")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int codigo;
    @Column(name = "NE_SIGLA")
    private String sigla;
    @Column(name = "NE_DESCRIPCION")
    private String descripcion;

    public NivelEducacion(String sigla, String descripcion) {
        this.sigla = sigla;
        this.descripcion = descripcion;
    }
}
