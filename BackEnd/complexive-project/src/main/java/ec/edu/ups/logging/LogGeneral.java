package ec.edu.ups.logging;

import ec.edu.ups.util.AccessControlFilter;
import lombok.*;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class LogGeneral {
    private static final String LOG_SEPARATOR = " || ";
    private static final String LOG_SEPARATOR_LAST = " - ";
    private static final String LOG_DEFF = "***************************************************************";
    private String uuidMicroservice = UUID.randomUUID().toString();
    private String userApp;
    private String ipUser;
    private String ipSource;
    private String uuidApp;
    private String action;
    private String process;
    private String request;
    private String response;
    private AccessControlFilter contextIn;
    private String dateTrn = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").format(new Date());
    private String guid;
    private String startProcess;
    private String endProcess;
    private String defaultStart;

    public LogGeneral(String userApp, String ipUser, String ipSource, String uuidApp, String action, String process, String request, String response) {
        this.userApp = userApp;
        this.ipUser = ipUser;
        this.ipSource = ipSource;
        this.uuidApp = uuidApp;
        this.action = action;
        this.process = process;
        this.request = request;
        this.response = response;
    }

    public LogGeneral(String action, String process, AccessControlFilter contextIn) {
        this.action = action;
        this.process = process;
        this.contextIn = contextIn;
        this.uuidMicroservice = contextIn.getLogGeneral().getUuidMicroservice();
    }

    public String getGuid() {
        return "UUID_MICROSERVICE: " + this.uuidMicroservice + LOG_SEPARATOR + "UUID_USER: " + this.uuidApp + LOG_SEPARATOR + "IP_USER: " + this.ipUser + LOG_SEPARATOR + "IP_SOURCE:" + this.ipSource + LOG_SEPARATOR + "USER:" + this.userApp + LOG_SEPARATOR + "Acción: " + this.action;
    }

    public String getStartProcess() {
        return "INICIA PROCESO DE " + this.process;
    }

    public String getEndProcess() {
        return "FINALIZA PROCESO DE " + this.process;
    }

    public String getDefaultStart() {
        return "Acción: " + this.action;
    }

    public String getDefaultStartUUID() {
        return this.getUUIdMicroservicio() + LOG_SEPARATOR + "Acción: " + this.action;
    }

    public String getUUIdMicroservicio() {
        return "UUID_MICROSERVICE: " + this.uuidMicroservice;
    }
}
