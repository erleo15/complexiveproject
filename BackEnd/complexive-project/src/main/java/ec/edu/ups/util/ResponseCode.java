package ec.edu.ups.util;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ResponseCode {

    OK_RESPONSE("200", "PETICION REALIZADA SATISFACTORIAMENTE", 200),
    ERROR_NOT_FOUND("404", "REGISTRO NO ENCONTRADO", 200),
    ERROR_NUMBERFORMAT_EX("400", "FORMATO INVALIDO EN LOS CAMPOS DE INGRESO", 400),
    ERROR_INTERNAL_SERVER("500", "ERROR INTERNO DEL SERVIDOR", 500);

    private final String responseCode;
    private final String message;
    private final int headerCode;
}
