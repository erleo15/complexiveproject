package ec.edu.ups.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.ArrayList;

@Data
@AllArgsConstructor
public class ApiPais {
    private ArrayList<Country> countries;

    private ApiPais() {
        countries = new ArrayList<>();
        countries.add(new Country());
    }
}
