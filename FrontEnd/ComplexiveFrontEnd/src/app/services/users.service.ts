import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { GetUsersI } from '../models-interfaces/getUsers';
import { NewPerson } from '../models-interfaces/postUsers';
import { UpdatePerson } from '../models-interfaces/putUsers';
import { GetUserById } from '../models-interfaces/getUserById';
import { ListNacionalidades } from '../models-interfaces/nacionalidades';
import { Promedio } from '../models-interfaces/promedios';
import { ListLevelEducation } from '../models-interfaces/nivelEducacion';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  usersGet = 'http://localhost:15010/con/person/basic';
  usersPost = 'http://localhost:15010/man/person/basic';
  usersPut = 'http://localhost:15010/man/person/basic';
 
  /* Nacionalidades */
  nacionalidadesGet = 'http://localhost:15010/con/nacionality/basic';
  /* Nacionalidades */
  nivelEducacionGet = 'http://localhost:15010/con/education/level/basic';

  /* Average */
  AverageGet = 'http://localhost:15010/con/average';

  constructor(
    private http: HttpClient
  ) { }
  
  getHeaders(language?: string) {
    let headers = new HttpHeaders();
    headers = headers.append('Content-Type', 'application/json; charset=utf-8');
    headers = headers.append('Accept', 'application/json');
    if(language) {
      headers = headers.append('Content-Language', `${language}`);
    }
    return { headers };
  }

  getUsers(body: GetUsersI) {
    return this.http.post(this.usersGet, body, this.getHeaders());
  }

  getAverageById(Id: string | null) {
    const bodyGeneral: Promedio = {
      "ipUser": "10..11.20",
      "uuidUser": "cae93b1b-953c-4f4d-a8c0-894cc5c76956-3",
      "userApp": "USR_WEB",
      "channel": "WEB",
      "operation": "CRUD CLIENT",
      "ctl": {
        "cNacionalidad": null
      }
    }
    bodyGeneral.ctl.cNacionalidad = Id;
    return this.http.post(this.AverageGet, bodyGeneral, this.getHeaders());
  }

  getAverage(body: Promedio) {
    return this.http.post(this.AverageGet, body, this.getHeaders());
  }

  getNacionalidades(body: ListNacionalidades) {
    return this.http.post(this.nacionalidadesGet, body, this.getHeaders());
  }

  getLevelsEducation(body: ListLevelEducation) {
    return this.http.post(this.nivelEducacionGet, body, this.getHeaders());
  }

  getUserById(body: GetUserById) {
    return this.http.post(this.usersGet, body, this.getHeaders());
  }

  postUsers(body: NewPerson) {
    return this.http.post(this.usersPost, body, this.getHeaders());
  }

  putUsers(body: UpdatePerson) {
    return this.http.put(this.usersPut, body, this.getHeaders());
  }
  
}
