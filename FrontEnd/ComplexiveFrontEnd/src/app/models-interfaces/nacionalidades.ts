export interface ListNacionalidades {
    ipUser: string;
    uuidUser: string;
    userApp: string;
    channel: string;
    operation: string;
    ctl: Ctl;
}

export interface Ctl {
    cNacionalidad: string;
    
}

export interface RespuestaNacionalidades {
    code: string;
    usr: string;
    dateTrn: string;
    uuidTrn: string;
    wsRef: string;
    msgUser: string;
    msgTech: string;
    ctl: CtlResponse;
}

export interface CtlResponse {
    nacionalidad: ArrayNacionalidad[];
}

export interface ArrayNacionalidad {
    codigo: number;
    paisNacionalidad: string;
    genticilioNacionalidad: string;
    isonacionalidad: string;
}