export interface UpdatePerson {
    ipUser: string;
    uuidUser: string;
    userApp: string;
    channel: string;
    operation: string;
    ctl: Ctl;
}

export interface Ctl {
    codigo: number | null;
    primerNombre: string;
    segundoNombre: string;
    apellidoPaterno: string;
    apellidoMaterno: string;
    fechaNacimiento: string;
    sexo: string;
    cNivelEducacion: number;
    cNacionalidad: number;
}