export interface GetUserById {
    ipUser: string;
    uuidUser: string;
    userApp: string;
    channel: string;
    operation: string;
    ctl: Ctl;
}

export interface Ctl {
    cPersona: string | null;
}