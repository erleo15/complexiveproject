export interface ListLevelEducation {
    ipUser: string;
    uuidUser: string;
    userApp: string;
    channel: string;
    operation: string;
    ctl: Ctl;
}

export interface Ctl {
    cNivelEducacion: string;    
}

export interface RespuestaLevelEducation {
    code: string;
    usr: string;
    dateTrn: string;
    uuidTrn: string;
    wsRef: string;
    msgUser: string;
    msgTech: string;
    ctl: CtlResponse;
}

export interface CtlResponse {
    nacionalidad: ArrayLevelEducation[];
}

export interface ArrayLevelEducation {
    codigo: number;
    sigla: string;
    descripcion: string;
}